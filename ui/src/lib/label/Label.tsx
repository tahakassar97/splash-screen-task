import { FC, LabelHTMLAttributes, ReactNode } from 'react';

interface Props
  extends Omit<LabelHTMLAttributes<HTMLLabelElement>, 'children'> {
  children: ReactNode;
}

const Label: FC<Props> = ({ children, ...props }) => {
  return <label {...props}>{children}</label>;
};

export { Label };
