import {
  FC,
  ReactNode,
  isValidElement,
  cloneElement,
  ReactElement,
  Children,
  useMemo,
  ButtonHTMLAttributes,
} from 'react';
import { FieldValues, SubmitHandler, useFormContext } from 'react-hook-form';

import { Button } from '../buttons';
import { withFormContext } from './hoc';

interface Props {
  className?: string;
  title?: string;
  children: ReactNode;
  Button?: ReactNode | null;
  record?: {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [key: string]: any;
  };
  onSubmit?: SubmitHandler<FieldValues>;
}

const Form: FC<Props> = ({
  children,
  Button: CustomButton,
  title = '',
  className,
  onSubmit = () => {
    //
  },
}) => {
  const { handleSubmit, watch } = useFormContext();

  const watchFields = watch();

  const requiredInputs = useMemo(() => {
    return Children.map(children, (child) => {
      if (isValidElement(child) && child.props.required) {
        const name = child.props.name;

        return name;
      }
    });
  }, [children]);

  const isDisabledButton = useMemo(() => {
    const result = requiredInputs?.some((input) => !watchFields[input]);

    return result;
  }, [requiredInputs, watchFields]);

  return (
    <form className={className} onSubmit={handleSubmit(onSubmit)}>
      {children}

      {CustomButton !== undefined ? (
        isValidElement(CustomButton) &&
        cloneElement(
          CustomButton as ReactElement<ButtonHTMLAttributes<HTMLButtonElement>>,
          {
            disabled: isDisabledButton,
            className: isDisabledButton
              ? 'btn bg-gray-400 w-full text-white cursor-not-allowed'
              : CustomButton.props.className,
          }
        )
      ) : (
        <Button className="mt-4 secondary-btn gap-x-1" type="submit">
          {title || 'Submit'}
        </Button>
      )}
    </form>
  );
};

const FormWrapper = withFormContext(Form);

export { FormWrapper as Form };
