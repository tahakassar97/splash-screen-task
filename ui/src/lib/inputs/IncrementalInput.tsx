import { FC, useEffect } from 'react';
import { useFormContext } from 'react-hook-form';

import { Label } from '../label';
import { Icon } from '../icons';
import { Button } from '../buttons';
import { BaseInput, BaseInputProps } from './BaseInput';

interface Props extends BaseInputProps {
  label: string;
  incrementAmount?: number;
  isFloat?: boolean;
}

const IncrementalInput: FC<Props> = ({
  label,
  incrementAmount = 1,
  isFloat,
  ...props
}) => {
  const { setValue, getValues } = useFormContext();

  const value = getValues(props.name);

  useEffect(() => {
    const defaultValue = props.defaultValue;

    if (defaultValue) {
      setValue(props.name, defaultValue);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.defaultValue, props.name]);

  const handleDecrement = () => {
    if (isFloat) {
      setValue(
        props.name,
        (
          parseFloat(value || props.defaultValue || 0) - incrementAmount
        ).toFixed(2)
      );

      return;
    }

    setValue(
      props.name,
      parseInt(value || props.defaultValue || 0) - incrementAmount
    );
  };

  const handleIncrement = () => {
    if (isFloat) {
      setValue(
        props.name,
        (
          parseFloat(value || props.defaultValue || 0) + incrementAmount
        ).toFixed(2)
      );

      return;
    }

    setValue(
      props.name,
      parseInt(value || props.defaultValue || 0) + incrementAmount
    );
  };

  return (
    <div className="p-2 rounded-md border border-gray-600 bg-gradient-to-r from-gray-900 to-gray-800">
      <Label className="flex justify-center text-xs text-text leading-none">
        {label}
      </Label>
      <div className="flex items-center justify-center gap-3 py-0.5 w-40">
        <Button
          type="button"
          className="flex justify-center items-center border rounded-md border-gray-500 w-6 h-6 cursor-pointer"
          onClick={handleDecrement}
        >
          <Icon
            name="arrow"
            className="text-white transform rotate-180"
            height="16"
            width="16"
          />
        </Button>

        <BaseInput {...props} className={`mt-0.5 ${props.className}`} />

        <Button
          type="button"
          className="flex justify-center items-center border rounded-md border-gray-500 w-6 h-6 cursor-pointer"
          onClick={handleIncrement}
        >
          <Icon name="arrow" className="text-white" height="16" width="16" />
        </Button>
      </div>
    </div>
  );
};

export { IncrementalInput };
