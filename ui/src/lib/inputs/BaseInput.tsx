import { FC, InputHTMLAttributes } from 'react';
import { useFormContext } from 'react-hook-form';

import classNames from 'classnames';

import { Label } from '../label';

export interface BaseInputProps extends Omit<InputHTMLAttributes<HTMLInputElement>, 'name'> {
  name: string;
  label?: string;
  containerClassName?: string;
}

const BaseInput: FC<BaseInputProps> = ({
  label,
  className = '',
  name,
  type,
  containerClassName = '',
  required = true,
  ...rest
}) => {
  const {
    register,
    formState: { errors },
  } = useFormContext();

  const inputError = errors[name];

  return (
    <div className={`flex flex-col relative ${containerClassName}`}>
      {label ? <Label className="w-full">{label}</Label> : null}

      <input
        className={classNames(`primary-input mt-1 ${className}`, {
          'ring-red-600': inputError,
        })}
        autoComplete="off"
        id={name}
        {...register(`${name}`, {
          ...(type === 'number' && { valueAsNumber: true }),
          required,
          max: rest.max,
        })}
        type={type}
        required={required}
        {...rest}
      />
    </div>
  );
};

export { BaseInput };
