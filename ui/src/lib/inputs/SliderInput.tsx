import { FC } from 'react';
import Slider, { SliderProps } from 'rc-slider';

import 'rc-slider/assets/index.css';

interface Props extends SliderProps {}

const SliderInput: FC<Props> = ({ ...props }) => {
  return <Slider {...props} />;
};

export { SliderInput };
