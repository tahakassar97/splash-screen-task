import { ButtonHTMLAttributes, FC, ReactNode } from 'react';

interface Props
  extends Omit<ButtonHTMLAttributes<HTMLButtonElement>, 'children'> {
  children: ReactNode;
}

const Button: FC<Props> = ({ children, ...props }) => {
  return <button {...props}>{children}</button>;
};

export { Button };
