import { FC } from 'react';
import { LineChart, Line, XAxis } from 'recharts';

interface Props {
  data: {
    title?: unknown;
    value?: unknown;
  }[];
  animationDuration?: number;
  onAnimationEnd?: VoidFunction;
  onAnimationStart?: VoidFunction;
}

const LineChartComponent: FC<Props> = ({
  data,
  animationDuration = 6000,
  onAnimationEnd,
  onAnimationStart,
}) => {
  return (
    <LineChart width={800} height={100} data={data}>
      <XAxis dataKey="title" domain={[0, 10]} />
      <Line
        dataKey="value"
        stroke="#8884d8"
        strokeWidth={2}
        dot={false}
        animationDuration={animationDuration}
        onAnimationStart={onAnimationStart}
        onAnimationEnd={onAnimationEnd}
      />
    </LineChart>
  );
};

export { LineChartComponent };
