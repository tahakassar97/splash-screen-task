/* eslint-disable @typescript-eslint/no-explicit-any */
import { Children, FC } from 'react';

import './styles.css';

interface Column {
  title: string;
  dataKey: string;
  className?: string;
}

interface Row {
  [key: string]: any;
}

interface Props {
  columns: Column[];
  data: Row[];
}

const Table: FC<Props> = ({ columns, data }) => {
  return (
    <div className="overflow-x-auto border border-gray-600 rounded-md">
      <table className="table-auto base-table w-full">
        <thead>
          <tr>
            {Children.toArray(
              columns.map((column) => (
                <th className={column.className}>{column.title}</th>
              ))
            )}
          </tr>
        </thead>
        <tbody>
          {Children.toArray(
            data.map((row) => (
              <tr>
                {Children.toArray(
                  columns.map((column) => (
                    <td className={`${row.className} ${column.className}`}>
                      {row[column.dataKey]}
                    </td>
                  ))
                )}
              </tr>
            ))
          )}
        </tbody>
      </table>
    </div>
  );
};

export { Table };
