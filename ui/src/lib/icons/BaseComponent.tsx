import { FC } from 'react';

import { Props } from './IProps';
import { Arrow } from './Arrow';

const Icon: FC<Props> = ({
  className,
  styles,
  height,
  width,
  name,
  viewBox,
  onClick,
}) => {
  const getIcon = () => {
    switch (name) {
      case 'arrow':
        return <Arrow />;
    }
  };

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      fill="currentColor"
      width={width || '25'}
      height={height || '25'}
      viewBox={`0 0 ${viewBox ? viewBox[0] : width || '25'} ${
        viewBox ? viewBox[1] : height || '25'
      }`}
      className={className}
      style={styles}
      onClick={onClick}
    >
      {getIcon()}
    </svg>
  );
};

export { Icon };
