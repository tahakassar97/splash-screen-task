import { CSSProperties } from 'react';

export interface Props {
  name: TIconName;
  className?: string;
  width?: string;
  height?: string;
  styles?: CSSProperties;
  viewBox?: [number, number];
  onClick?: () => void;
}

export type TIconName = 'arrow';
