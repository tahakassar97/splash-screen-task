export enum Colors {
  PRIMARY = '#ef5b98',
  SECONDARY = '#fb806a',
  TEXT = '#8b919f',
}
