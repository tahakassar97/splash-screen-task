import { StrictMode } from 'react';
import * as ReactDOM from 'react-dom/client';

import { Main } from '../src/app/pages/index';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <StrictMode>
    <Main />
  </StrictMode>
);
