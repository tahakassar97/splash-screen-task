import { useState, useEffect } from 'react';

const useDigitalHour = () => {
  const [digitalHour, setDigitalHour] = useState('');

  useEffect(() => {
    const interval = setInterval(() => {
      const time = new Date();
      const hours = String(time.getHours()).padStart(2, '0');
      const minutes = String(time.getMinutes()).padStart(2, '0');
      setDigitalHour(`${hours}:${minutes}`);
    }, 60000);

    return () => clearInterval(interval);
  }, []);

  return digitalHour;
};

export { useDigitalHour };
