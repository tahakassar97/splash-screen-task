import { FC, useMemo, useState } from 'react';
import { FieldValues } from 'react-hook-form';

import {
  BaseInput,
  Button,
  Form,
  IncrementalInput,
  Label,
  LineChartComponent,
  SliderInput,
  Table,
} from '@org/ui';
import { Colors } from '@org/constants';

import {
  MULTIPLIER,
  Players,
  START_PLAYERS_POINTS,
  chartData,
  generateCurrentRoundData,
  generateInitCurrentRoundData,
  generateInitTotalRoundsData,
  generateRandomNumber,
  sliderInputMarks,
} from './constants';
import { DetailsCard } from './components';
import { useDigitalHour } from './hooks';
import { RoundDataProps } from './interfaces';
import classNames from 'classnames';

const Main: FC = () => {
  const [playerName, setPlayerName] = useState('');
  const [sliderValue, setSliderValue] = useState(1);
  const [isRenderChart, setIsRenderChart] = useState(false);
  const [predicateValue, setPredicateValue] = useState(-1);
  const [chartDataValue, setChartDataValue] = useState(chartData);
  const [isLoading, setIsLoading] = useState(false);

  const [currentRoundData, setCurrentRoundData] = useState<RoundDataProps[]>(
    generateInitCurrentRoundData()
  );

  const handleSubmit = (data: FieldValues) => {
    setPlayerName(data.name);
  };

  const totalRoundsData = generateInitTotalRoundsData();

  const handleStartRound = (data: FieldValues) => {
    const result = totalRoundsData.map((currentRoundItem) => {
      if (currentRoundItem.name !== Players.YOU) {
        return {
          ...generateCurrentRoundData(
            currentRoundItem.name,
            currentRoundItem.points >= 0
              ? currentRoundItem.points
              : START_PLAYERS_POINTS,
            MULTIPLIER
          ),
        };
      }

      return {
        name: currentRoundItem.name,
        multiplier: data.multiplier,
        points: data.points,
      };
    });

    const randomNumber = generateRandomNumber(MULTIPLIER);
    const updatedData = chartDataValue.map((value, i) => ({
      title: value.title,
      value: i > randomNumber ? undefined : value.value,
    }));

    setCurrentRoundData(result);
    setIsRenderChart(true);
    setChartDataValue(updatedData);
    setPredicateValue(randomNumber);
  };

  const digitalHour = useDigitalHour();

  const totalPoints = useMemo(() => {
    const res = totalRoundsData.find(
      (player) => player.name === Players.YOU
    )?.points;

    return res === -1 ? START_PLAYERS_POINTS : res;
  }, [totalRoundsData]);

  return (
    <div className="bg-[#15191f] h-screen">
      <div className="container mx-auto px-12 py-5">
        <div className="flex gap-5">
          {!playerName ? (
            <span className="flex flex-col justify-between py-24 px-5 rounded-md bg-panel-bg h-full border border-gray-600 w-[30%]">
              <Label className="text-text font-bold text-center text-xl">
                Welcome
              </Label>

              <Form
                Button={<Button className="primary-btn w-full">Accept</Button>}
                onSubmit={handleSubmit}
              >
                <Label className="flex justify-center text-center text-text text-xs mb-0.5">
                  Please insert your name
                </Label>

                <BaseInput
                  name="name"
                  className="base-input mb-5"
                  required
                  minLength={3}
                />
              </Form>

              <div />
            </span>
          ) : (
            <span className="flex flex-col p-5 h-full w-[30%]">
              <Form
                Button={
                  <Button
                    className={classNames('btn w-full', {
                      'text-gray-500': isLoading,
                      'primary-btn': !isLoading,
                    })}
                    disabled={isLoading}
                  >
                    Start
                  </Button>
                }
                onSubmit={handleStartRound}
              >
                <div className="flex w-full justify-between gap-3 mb-4">
                  <IncrementalInput
                    label="Points"
                    name="points"
                    className="base-input w-20 !px-2 !py-0 text-sm text-center !border-0"
                    defaultValue={50}
                    incrementAmount={25}
                  />
                  <IncrementalInput
                    label="Multiplier"
                    name="multiplier"
                    className="base-input w-20 !px-2 !py-0 text-sm text-center !border-0"
                    defaultValue={2}
                    isFloat
                    incrementAmount={0.25}
                  />
                </div>
              </Form>

              <Label className="text-text font-bold mt-5 mb-2">
                Current Round
              </Label>
              <Table
                columns={[
                  {
                    dataKey: 'name',
                    title: 'Name',
                    className: 'px-4 py-1 text-center text-white text-xs',
                  },
                  {
                    dataKey: 'points',
                    title: 'Points',
                    className: 'px-4 py-1 text-center text-white text-xs',
                  },
                  {
                    dataKey: 'multiplier',
                    title: 'Multiplier',
                    className: 'px-4 py-1 text-center text-white text-xs',
                  },
                ]}
                data={currentRoundData}
              />

              <div className="mt-6">
                <Label className="font-bold text-text">Speed</Label>
                <div className="pt-5 pb-8 px-4 bg-gray-600 mt-1 bg-opacity-10 rounded-md">
                  <SliderInput
                    step={null}
                    min={20}
                    styles={{
                      rail: {
                        backgroundColor: 'gray',
                      },
                      track: {
                        backgroundColor: Colors.PRIMARY,
                      },
                      handle: {
                        backgroundColor: Colors.PRIMARY,
                      },
                    }}
                    marks={{ ...sliderInputMarks }}
                    onChange={(value) => {
                      setSliderValue(value as number);
                    }}
                  />
                </div>
              </div>
            </span>
          )}
          <span className="w-[68%] mt-5">
            <ul className="flex gap-5 justify-between w-full">
              <DetailsCard>
                {playerName ? (
                  <div className="text-center text-white font-bold">
                    {totalPoints}
                  </div>
                ) : null}
              </DetailsCard>
              <DetailsCard>
                <div className="text-center text-white font-bold">
                  {playerName ?? null}
                </div>
              </DetailsCard>
              <DetailsCard>
                <div className="text-center text-white font-bold">
                  {playerName ? digitalHour : null}
                </div>
              </DetailsCard>
            </ul>
            <div className="flex justify-center h-[20dvh] bg-panel-bg rounded-md border-gray-500 p-5 mt-6">
              <Label className="text-lg text-text font-bold">
                {predicateValue > -1 ? predicateValue : ''}
              </Label>
              {isRenderChart ? (
                <LineChartComponent
                  data={chartDataValue}
                  animationDuration={sliderValue * 100}
                  onAnimationStart={() => {
                    setIsLoading(true);
                  }}
                  onAnimationEnd={() => {
                    if (isLoading) {
                      const generatedData = currentRoundData.map(
                        (roundData) => ({
                          ...roundData,
                          points:
                            +roundData.multiplier <= predicateValue
                              ? +roundData.points * +roundData.multiplier
                              : 0,
                          className:
                            +roundData.multiplier <= predicateValue
                              ? 'text-green-500'
                              : 'text-red-500',
                        })
                      );
                      setCurrentRoundData(generatedData);
                      setIsLoading(false);
                    }
                  }}
                />
              ) : null}
            </div>
          </span>
        </div>
        <div></div>
      </div>
    </div>
  );
};

export default Main;
