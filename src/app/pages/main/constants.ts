import { ChartData } from './interfaces';

const step = 20;
export enum Players {
  YOU = 'You',
  CPU1 = 'CPU1',
  CPU2 = 'CPU2',
  CPU3 = 'CPU3',
  CPU4 = 'CPU4',
}

export const players = Object.values(Players);

export const sliderInputMarks = Array.from(
  { length: 5 },
  (_, index) => (index + 1) * step
).reduce((prevValue, currentValue) => {
  return {
    ...prevValue,
    [currentValue]: `${currentValue / step}×`,
  };
}, {});

export const START_PLAYERS_POINTS = 1000;
export const MULTIPLIER = 10;

export const generateRandomNumber = (max = 1000) => {
  return Math.floor(Math.random() * max);
};

export const generateInitCurrentRoundData = () => {
  const data = players.map((player) => ({
    name: player,
    points: '-',
    multiplier: '-',
    className: '',
  }));

  return data;
};

export const generateInitTotalRoundsData = () => {
  const data = players.map((player) => ({
    name: player,
    points: -1,
  }));

  return data;
};

export const generateCurrentRoundData = (
  player: Players,
  points: number,
  multiplier: number
) => {
  return {
    name: player,
    points: generateRandomNumber(points + 1),
    multiplier: generateRandomNumber(multiplier + 1),
  };
};

export const chartData: ChartData[] = [
  {
    title: '1',
    value: 0,
  },
  {
    title: '2',
    value: 1,
  },
  {
    title: '3',
    value: 2,
  },
  {
    title: '4',
    value: 3,
  },
  {
    title: '5',
    value: 4,
  },
  {
    title: '6',
    value: 5,
  },
  {
    title: '7',
    value: 6,
  },
  {
    title: '8',
    value: 7,
  },
  {
    title: '9',
    value: 8,
  },
  {
    title: '10',
    value: 9,
  },
];
