import { Players } from '../constants';

export interface RoundDataProps {
  name: Players;
  points: number | string;
  multiplier: number | string;
}

export interface TotalDataProps {
  name: Players;
  points: number;
}
