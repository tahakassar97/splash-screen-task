import { FC, ReactNode } from 'react';

interface Props {
  children: ReactNode;
}

const DetailsCard: FC<Props> = ({ children }) => {
  return (
    <li className="p-2 w-full rounded-md border border-gray-600 bg-gradient-to-r from-gray-900 to-gray-800">
      {children}
    </li>
  );
};

export { DetailsCard };
